# Ana Williamson Architect

## DOCKER INFO

### **To get things running locally**

+ install docker (you may need to create a docker hub account)

+ map awa.yes-and.net to your localhost by editing /etc/hosts on your local machine:

   `127.0.0.1 awa.yes-and.net`

+ from inside the awasite directory, run (make sure to include the . at the end):
    
    `docker build --rm -t webserver .`

+ then run the command below. This names the image nginx, mounts it to 80:80 and binds the contents of the /dist folder from your computer to the /var/www folder in docker

  `docker run -d -p 80:80 --name nginx --mount type=bind,source="$(pwd)"/dist,target=/var/www webserver`


### **Useful commands**

+ `docker ps -a`  will list running containers
+ `docker stop container_name`  will stop a running container by name, in our case it's nginx
+ `docker start container_name`  will start a running container by name, in our case it's nginx
+ `docker logs container_name`  will spit out logs of the container
+ `docker system prune`  will delete all stopped containers (useful if you mess things up and want to start over)
+ `docker exec -it container_name bash` to ssh into docker container


### **Apache Config**

`RewriteEngine On`
`RewriteBase /`
`RewriteCond %{REQUEST_FILENAME} !-f`
`RewriteCond %{REQUEST_FILENAME} !-d`
`RewriteRule . /index.html [L]`


### NODE

* **NOTE:** use version ~14 as anything newer breaks SASS
* Had to do `npm run build` (it puts files in the root directory) vs `npm run webpack` seems broken on ubuntu mate
