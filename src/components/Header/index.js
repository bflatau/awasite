import React from 'react';
import { connect } from 'react-redux';
import AppHeader from './AppHeader';
import { withRouter } from 'react-router';
import { menuItemSelected } from '../../actionCreators';

const AppHeaderContainer = ({ app, mainNavBar }) => (
  <AppHeader
    app={app}
    mainNavBar={mainNavBar}
    menuItemSelected={menuItemSelected}
  />
);

const mapStateToProps = ({ app, mainNavBar }) => ({
  app,
  mainNavBar,
});

export default withRouter(connect(mapStateToProps)(AppHeaderContainer));
