import createReducer from '../utils/createReducer';
import { MENU_ITEM_SELECTED } from '../constants/actions';

const initialState = {
  menuItems: [
    { title: 'PORTFOLIO', redirectURL: '/portfolio' },
    { title: 'PROCESS', redirectURL: '/process' },
    { title: 'STUDIO', redirectURL: '/studio' },
    { title: 'NEWS', redirectURL: '/news' },
    { title: 'CONTACT', redirectURL: '/contact' },
  ],

  selectedMenuItem: { title: 'home', redirectURL: '/' },
};

export default createReducer(initialState, {
  [MENU_ITEM_SELECTED]: (state, { data: { itemName: selectedMenuItem } }) => ({
    ...state,
    selectedMenuItem,
  }),
});
