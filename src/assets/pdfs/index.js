export { default as sunsetPDF } from '../pdfs/Sunset_Article.pdf';
export { default as paloAltoWeeklyPDF } from '../pdfs/palo_alto_weekly.pdf';
export { default as predesignQuestionnaire } from '../pdfs/AWA-Predesign-Questionnaire-Form.pdf';
export { default as projectPlanningPack } from '../pdfs/AWA-Project-Planning-Pack.pdf';
export { default as seniorDesigner } from '../pdfs/awa_senior_designer.pdf';
export { default as juniorDesigner } from '../pdfs/awa_junior_designer.pdf';
export { default as projectManager } from '../pdfs/awa_project_manager.pdf';
